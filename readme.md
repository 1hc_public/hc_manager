<!-- hc_scraper documentation master file, created by
sphinx-quickstart on Thu Feb 18 01:12:35 2021.
You can adapt this file completely to your liking, but it should at least
contain the root `toctree` directive. -->
# Welcome to hc_manager’s documentation!

# Manager (Distributed)


### class hc_manager.manager.distributed_manager.Manager()
Manage distributed operations.


#### add_main_s3(df)
Upload primary dataframe to S3 if not exists.


#### form_image(img_name, set_aws_profile=True)
Build img & send additional files and args through ssh.


#### gather_results()
Gather scraped results.


#### load_targets()
Load args from col 0 for parallel / dist scrape.


#### run_script()
Run script through pssh.


#### set_build_args(args)
Set additional bash commands for image build.


#### set_s3_root(name_bucket, name_dir)
Set/create S3 bucket and output directory.


#### teardown_bucket()
Remove all files and delete bucket.


#### teardown_image()
Terminate and delete image.


#### terminate_image_instance()
Terminates instance image built from.

# Scrape


### class hc_manager.scrape.base.Scrape(path_driver=None, update_driver=False, driver_kwargs={'headless': True})
Simplify common scraping operations.


#### add_operation(fx)
Add an operation to be executed.


#### execute_operations()
Execute all operations in queue.


#### load_target(target_url)
Load target for scrape.
