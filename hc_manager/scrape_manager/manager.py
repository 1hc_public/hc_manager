"""Module to manage scrape."""
import os
import sys
import time

import pandas as pd

from hc_manager.scrape_google.scrape_target import Target
from hc_manager.scrape.base import Scrape

DOCKER_ENV = os.system('sudo pwd') != 0

ARGS_BOOL = len(sys.argv) > 1
LOCAL = len(sys.argv) <= 1

if ARGS_BOOL:
    machine_num = int(sys.argv[1])
else:
    machine_num = 0


class Manager:
    """Manage job at highest level."""

    def __init__(self, s3_bucket=None,
                 ami=None, teardown=True):
        """Initialize manager."""
        self.mgr = Scrape(
            'hc_google_scrape',
            machine_num=machine_num,
            update_driver=False
            ) if DOCKER_ENV or LOCAL \
            else Scrape('hc_google_scrape',
                        machine_num=machine_num,
                        update_driver=True)
        self.mgr.set_s3_root('hc-google-scrape', 'collection')  # S3
        self.build_up(ami=ami)
        # TODO: collect_results, update_main_s3, write_output
        if not LOCAL:
            self.run_targets()

        if LOCAL:
            self.mgr.load_targets()
            self.mgr.build_cluster(
                self.mgr.img.img_id,
                4
                )
            time.sleep(300)
            for i in self.mgr.img.instances:
                i.update()

            self.build_argv()
            self.run_remote()
            # self.mgr.img.terminate_instances()

            # if self.teardown:
            #     self.teardown()

    def build_up(self, ami=None):
        """Build job preparation."""
        if LOCAL:
            """Build/get image."""
            # IMG
            if not ami:
                install_scrape = ''.join([
                            'pip install hc-manager --extra-index-url ',
                            'https://__token__:dan1hc@gitlab.com',
                            '/api/v4/projects/',
                            '24796864/packages/pypi/simple'
                                ])  # TODO: Testing purposes, install self.

                self.mgr.set_build_args(
                    [install_scrape]
                    )

                self.mgr.form_image('hc_google_scrape')
                time.sleep(90)  # Leave some extra time for image to form.
                # Clear instance cache.
                self.mgr.img.terminate_instances()
                self.mgr.img.instances = []
            else:
                self.mgr.img.img_id = ami

            """Upload/get df_main."""
            self.df_main = pd.read_csv(
                '~/1hc_public/entity_matching/entities.csv',
                index_col=0
                ) \
                .sample(int(4e2))

            self.mgr.add_main_s3(self.df_main)

        if not LOCAL:
            self.build_targets()

        if LOCAL:
            self.mgr.load_targets()
            self.mgr.build_cluster(
                self.mgr.img.img_id,
                4
                )
            self.build_argv()

    def build_targets(self):
        """Build args to be passed to scrape constructor."""
        # For each item in df_main, construct scrape args.
        self.mgr.read_main()
        self.targets = []
        for tup in self.mgr.df_main.fillna('').itertuples():
            write_loc = tup[1].replace('/', '||')
            search_text = ' '.join([s for s in tup[2:4]])
            tgt = Target(
                f'collection/{write_loc}/df.csv',
                search_text,
                self.mgr.img.bucket
                )
            tgt.build_actions()
            self.targets.append(
                tgt
                )

    def build_argv(self):
        """Build command args to be passed to pclient."""
        argv = []
        for ix, instance in enumerate(self.mgr.img.instances):
            _argv = ''.join([
                'python -c "from hc_manager.scrape_manager import manager;',
                f'mgr = manager.Manager()" {ix}'
                ])
            argv.append(_argv)
        self.argv = argv

    def run_targets(self):
        """Run full scrape on targets."""
        for tgt in self.targets:
            tgt.execute_operations()
            time.sleep(3)  # Avoid rate limiting.

    def run_remote(self):
        """Run all scrapes."""
        return self.mgr.run_script(
            self.argv
            )

    def teardown(self):
        """Teardown both image and bucket."""
        self.mgr.teardown_image()
        self.mgr.teardown_bucket()
