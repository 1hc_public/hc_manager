"""Module to scrape."""
import pandas as pd
from selenium.webdriver.common.keys import Keys

from hc_manager.scrape.base import Scrape


class Target(Scrape):
    """Modify the functionality of this scrape class."""

    def __init__(self, write_loc, search_term, s3_bucket_name,
                 path_driver=None, update_driver=False,
                 test_env=False, driver_kwargs={'headless': True}):
        """Initialize scrape."""
        super().__init__(
            s3_bucket_name,
            path_driver=path_driver,
            update_driver=update_driver,
            driver_kwargs=driver_kwargs,
            test_env=test_env
            )
        self.write_loc = write_loc
        self.search_term = search_term
        self.form = None
        self.corpus = []

    def load_google(self, url='https://www.google.com'):
        """Load Google homepage."""
        self.load_target(url)

    def get_search_input(self):
        """Get search input element."""
        self.form = [
            e for e in self.driver.find_elements_by_tag_name('input')
            if e.get_attribute('Title') == 'Search'
            ][0]

    def search_text(self):
        """Search Google for provided text."""
        self.form.send_keys(self.search_term)
        self.form.send_keys(Keys.RETURN)

    def get_page_summaries(self):
        """Get page text."""
        self.corpus = self.driver \
            .find_element_by_tag_name('body').text.split('\n')

    def cleanse_page_summaries(self):
        """Ensure we are collecting actual summaries."""
        N = 140
        if self.corpus:
            self.corpus = [
                s for s in self.corpus if
                len(s) >= N
                ]

        self.data = pd.Series(
            self.corpus
            ).reset_index(drop=False) \
            .rename(columns={
                'index': 'google_index',
                0: 'corpus'
                })

        self.data['url'] = self.driver.current_url

        return self.data

    def build_actions(self):
        """Packages actions for super executor."""
        self.add_operation(self.load_google)
        self.add_operation(self.get_search_input)
        self.add_operation(self.search_text)
        self.add_operation(self.get_page_summaries)
        self.add_operation(self.cleanse_page_summaries)
        self.add_operation(self.write_results)

    def check_requirements(self):
        """Check if results match specified reqs."""
        N = 140

        if self.data.empty:
            # Check if data
            return self.data
        elif self.data.url.dropna().empty:
            # Check if url
            return pd.DataFrame()
        elif len(self.data.corpus.iloc[0]) < N:
            # Check if text long enough
            return pd.DataFrame()
        else:
            return self.data

    def write_results(self, form='csv'):
        """Write QA'd results to specified location."""
        df = self.check_requirements()

        if df.empty:
            return

        # Check versus current frame if exists.
        try:
            df_existing = pd.read_csv(self.write_loc)
        except Exception:
            df_existing = pd.DataFrame()

        if len(df_existing) > len(df):
            return

        # Write to file where all checks passed.
        if not form or form == 'csv':
            self.aws.write_pandas(df, self.write_loc)
