"""Module to manage scrape."""
import os
import sys
import time

import pandas as pd

from hc_manager.scrape_google.scraper.scrape_target import Target
from hc_manager.scrape.base import Scrape

DOCKER_ENV = os.system('sudo pwd') != 0

ARGS_BOOL = len(sys.argv) > 1
LOCAL = len(sys.argv) <= 1

if ARGS_BOOL:
    machine_num = int(sys.argv[1])
else:
    machine_num = 0


class Manager:
    """Manage job at highest level."""

    def __init__(self, s3_bucket=None,
                 ami=None, teardown=True):
        """Initialize manager."""
        self.mgr = Scrape(
            'hc_google_scrape',
            machine_num=machine_num,
            update_driver=False,
            )
        self.SIZE = 4
        self.mgr.set_s3_root('hc-google-scrape', 'collection')  # S3
        self.build_up(ami=ami)
        # TODO: collect_results, update_main_s3, write_output
        if not LOCAL:
            self.run_targets()

        if LOCAL:
            self.mgr.load_targets()
            self.mgr.build_cluster(
                self.mgr.aws.img_id,
                self.SIZE
                )
            time.sleep(300)  # Leave some extra boot time.
            for i in self.mgr.aws.instances:
                i.update()

            self.build_argv()
            self.run_remote()
            self.mgr.aws.terminate_instances()

            # if self.teardown:
            #     self.teardown()

    def build_up(self, ami=None):
        """Build job preparation."""
        if LOCAL:
            """Upload/get df_main."""
            self.df_main = pd.read_csv(
                '~/1hc_public/entity_matching/entities.csv',
                index_col=0
                ) \
                .sample(int(1e2))
            self.add_main_s3()

            """Build/get image."""
            # IMG
            if not ami:
                install_aws = ''.join([
                            'pip uninstall aws-manager -y && '
                            'pip install aws-manager --extra-index-url ',
                            'https://__token__:dan1hc@gitlab.com',
                            '/api/v4/projects/',
                            '24630515/packages/pypi/simple'
                                ])
                install_base = ''.join([
                            'pip install hc-scraper --extra-index-url ',
                            'https://__token__:dan1hc@gitlab.com',
                            '/api/v4/projects/',
                            '24630370/packages/pypi/simple'
                                ])
                install_scrape = ''.join([
                            'pip install hc-manager --extra-index-url ',
                            'https://__token__:dan1hc@gitlab.com',
                            '/api/v4/projects/',
                            '24796864/packages/pypi/simple'
                                ])  # TODO: Testing purposes, install self.

                self.mgr.set_build_args(
                    [install_aws, install_base, install_scrape]
                    )
                self.mgr.aws.append_install_script([
                    ''.join([
                        'python3 -c "from hc_scraper.update ',
                        'import update_chromedriver;',
                        'update_chromedriver.update()"'
                    ])])

                self.mgr.form_image('hc_google_scrape')
                time.sleep(180)  # Leave some extra time for image to form.
                # Clear instance cache.
                self.mgr.aws.terminate_instances()
                self.mgr.aws.instances = []
            else:
                self.mgr.aws.img_id = ami

        if not LOCAL:
            self.build_targets()

        if LOCAL:
            self.mgr.load_targets()

    def build_targets(self):
        """Build args to be passed to scrape constructor."""
        # For each item in df_main, construct scrape args.
        self.mgr.read_main()
        self.targets = []
        for tup in self.mgr.df_main.fillna('').itertuples():
            write_loc = tup[1].replace('/', '||')
            search_text = ' '.join([s for s in tup[2:4]])
            tgt = Target(
                f'collection/{write_loc}/df.csv',
                search_text,
                self.mgr.aws.bucket
                )
            tgt.build_actions()
            self.targets.append(
                tgt
                )

    def build_argv(self):
        """Build command args to be passed to pclient."""
        argv = []
        for ix, instance in enumerate(self.mgr.aws.instances):
            _argv = ''.join([
                'python3 -c "from hc_manager.scrape_google.manager',
                ' import manager;',
                f'mgr = manager.Manager()" {ix}'
                ])
            argv.append(_argv)
        self.argv = argv

    def add_main_s3(self):
        """Cleanse & upload primary dataframe to S3 if not exists."""
        self.df_main['machine_num'] = None
        splits = len(self.df_main) // self.SIZE
        for _ix in range(self.SIZE):
            self.df_main['machine_num'].iloc[_ix * splits] = _ix

        self.df_main['machine_num'] = self.df_main['machine_num'].ffill()

        self.mgr.aws.write_pandas(self.df_main, 'df_main.csv')

    def run_targets(self):
        """Run full scrape on targets."""
        for tgt in self.targets:
            tgt.execute_operations()
            time.sleep(3)  # Avoid rate limiting.

    def run_remote(self):
        """Run all scrapes."""
        return self.mgr.run_script(
            self.argv
            )

    def teardown(self):
        """Teardown both image and bucket."""
        self.mgr.teardown_image()
        self.mgr.teardown_bucket()
