"""Module with scraping base class."""
import os

import pandas as pd

from hc_scraper import scrape, update
from hc_manager.manager.distributed_manager import Manager


class Scrape(Manager):
    """Simplify common scraping operations."""

    def __init__(self, s3_bucket_name, machine_num=None,
                 path_driver=None, update_driver=False,
                 driver_kwargs={'headless': True}, test_env=False):
        """Initialize driver and data store (pandas)."""
        super().__init__()  # Init AWS_Manager.
        if update_driver:
            update.update_chromedriver.update()

        _PATH_ROOT = os.path.expanduser('~')
        PATH_DRIVER = f'{_PATH_ROOT}/webdriver/chromedriver'

        self.path_driver = path_driver if path_driver else PATH_DRIVER
        self.test_env = test_env
        self.operation_queue = []
        self.data = pd.DataFrame()
        self.machine_num = machine_num if machine_num else 0
        self.set_s3_root(
            s3_bucket_name,
            'collection',
            )

    def init_driver(self):
        """Initialize webdriver."""
        self.driver = scrape.scraper.start_driver(
            path_driver=self.path_driver,
            test_env=self.test_env
            )

    def close_driver(self):
        """Close selenium."""
        self.driver.close()

    def read_main(self):
        """Load all targets for machine from s3."""
        # TODO: Tests?
        self.df_main = self.aws.read_pandas('df_main.csv')
        self.df_main = self.df_main[
            self.df_main.iloc[:, -1] == self.machine_num
            ]  # Filter to assignment.

    def load_target(self, target_url):
        """Load target for scrape."""
        self.driver.get(target_url)

    def add_operation(self, fx):
        """Add an operation to be executed."""
        self.operation_queue.append(fx)

    def execute_operations(self):
        """Execute all operations in queue."""
        self.init_driver()

        for op in self.operation_queue:
            r = op()

            if isinstance(r, pd.core.frame.DataFrame):
                if not r.empty:
                    self.data = self.data.append(r)

        self.close_driver()
