"""Module to manage distributed scrape."""
import time

from aws_manager.img.image_manager import Image


class Manager:
    """Manage distributed operations."""

    def __init__(self):
        """Connect to AWS."""
        self.aws = Image()

    def set_build_args(self, args):
        """Set additional bash commands for image build."""
        self.build_args = args
        self.aws.append_install_script(
            self.build_args
            )

    def form_image(self, img_name, set_aws_profile=True):
        """Build img & send additional files and args through ssh."""
        if set_aws_profile:
            self.aws.append_install_script([
                'sudo mkdir /home/ubuntu/.aws',
                ''.join([
                    'echo -e ',
                    '"[default]\nregion = ',
                    self.aws.ec2.region.name,
                    '" > /home/ubuntu/.aws/config'
                    ]),
                ''.join([
                    'echo -e ',
                    '"[default]\naws_access_key_id = ',
                    self.aws.aws_access_key,
                    '\naws_secret_access_key = ',
                    self.aws.aws_secret,
                    '" > /home/ubuntu/.aws/credentials'
                    ])
                ])

        self.aws.build_image()
        time.sleep(180)  # Leave time for information.txt to finish.
        self.aws.register_image(img_name)
        time.sleep(90)  # Leave time for image to register.
        self.aws.init_pclient()

    def terminate_image_instance(self):
        """Terminates instance image built from."""
        self.aws.terminate_instances()

    def teardown_image(self, image_id=None):
        """Terminate and delete image."""
        return self.aws.delete_image(image_id) \
            if image_id else self.aws.delete_image(
                    self.aws.img_id
                    )

    def set_s3_root(self, name_bucket, name_dir):
        """Set/create S3 bucket and output directory."""
        try:
            self.aws.build_bucket(name_bucket)
        except Exception:  # Bucket already exists.
            self.aws.set_bucket(name_bucket)
        self.collection_dir = name_dir

    def load_targets(self):
        """Load args from col 0 for parallel / dist scrape."""
        self.df = self.aws.read_pandas('df_main.csv')
        return self.df

    def teardown_bucket(self):
        """Remove all files and delete bucket."""
        for k in self.df.iloc[:, 0]:
            try:
                k = k.replace('/', '|')  # Cleanse problem_chars.
            except Exception:  # Who cares.
                pass

            if self.aws.exists(
                    f'{self.collection_dir}/{k}/df.csv'
                    ):
                self.aws.delete_file(f'{self.collection_dir}/{k}/df.csv')
        self.aws.delete_file('df_main.csv')
        return self.aws.delete_bucket(self.aws.bucket)

    def build_cluster(self, ami, size, instance_type='t3.micro'):
        """Build cluster of AMIs."""
        return self.aws.boot_instances(
            ami,
            instance_type=instance_type,
            instance_count=size
            )

    # TODO 2
    def run_script(self, args, sleep_time=9e2):
        """Run script through pssh."""
        self.aws.init_pclient()
        self.shells = self.aws.pclient.open_shell()
        return self.aws.pclient.run_shell_commands(
            self.shells,
            args
            )
        # time.sleep(int(sleep_time))

    # TODO 3
    def gather_results(self):
        """Gather results."""
