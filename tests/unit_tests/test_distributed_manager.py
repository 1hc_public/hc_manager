"""Core remote functionality tests."""
import unittest
import os

import pandas as pd

from hc_manager.manager.distributed_manager import Manager


DOCKER_ENV = os.system('sudo pwd') != 0
mgr = Manager()


class TestBuildArgs(unittest.TestCase):
    """Test if args set correctly."""

    def test_set_build_args(self):
        """Test if args set."""
        install_scrape = ''.join([
            'pip install scrape-google --extra-index-url ',
            'https://__token__:dan1hc@gitlab.com/api/v4/projects/',
            '24796431/packages/pypi/simple'
                ])

        mgr.set_build_args(
            [install_scrape]
            )

        self.assertTrue(
            install_scrape in mgr.aws.information
            )


class TestFormImage(unittest.TestCase):
    """Test if image forms correctly."""

    def test_form_image(self):
        """Test if image forms."""
        mgr.form_image('hc_default_test')
        self.assertIsNotNone(
            mgr.aws.img_id
            )


class TestFormCreds(unittest.TestCase):
    """Test if aws creds passed."""

    def test_form_credentials(self):
        """Test if credentials correctly echoed."""
        if DOCKER_ENV:
            self.assertTrue(True)
        else:
            mgr.aws.init_pclient()  # Re-init for test env.
            _r = mgr.aws.pclient.run_command('cat ~/.aws/credentials')[0]
            output = [l for l in _r.stdout]
            self.assertIsNotNone(output)


class TestFormPackage(unittest.TestCase):
    """Test if package installed."""

    def test_form_package(self):
        """Test if package passed."""
        if DOCKER_ENV:
            self.assertTrue(True)
        else:
            mgr.aws.init_pclient()  # Re-init for test env.
            _r = mgr.aws.pclient.run_command('python import scrape_google')[0]
            output = [l for l in _r.stdout]
            self.assertEqual(output, [])


class TestSetS3Root(unittest.TestCase):
    """Test if root and collection dir set."""

    def test_set_s3_root(self):
        """Test if s3 bucket and collection dir exist."""
        mgr.set_s3_root('hc-default-test', 'collection')
        self.assertEqual(
            mgr.aws.bucket,
            'hc-default-test'
            )
        self.assertEqual(
            mgr.collection_dir,
            'collection'
            )


class TestAddMainS3(unittest.TestCase):
    """Test if df uploads correctly."""

    def test_add_main_s3(self):
        """Test upload, coverage really through read."""
        mgr.aws.write_pandas(
            pd.DataFrame([[*range(5)] for i in range(5)]),
            'df_main.csv'
            )
        self.assertTrue(True)


class TestLoadTargets(unittest.TestCase):
    """Test if df uploaded correctly."""

    def test_load_targets(self):
        """Test if our df is our df."""
        mgr.load_targets()
        self.assertEqual(
            len(mgr.df),
            5
            )


class TestTeardown(unittest.TestCase):
    """Test teardown."""

    def test_teardown_image(self):
        """Test if image deregisters and instance terminates."""
        self.assertTrue(
            mgr.teardown_image()
            )

    def test_teardown_bucket(self):
        """Test whether client returns correct status code."""
        r = mgr.teardown_bucket()
        s = r['ResponseMetadata']['HTTPStatusCode']
        self.assertEqual(
            str(s)[0],
            '2'
            )
