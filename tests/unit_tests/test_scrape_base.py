"""Core functionality tests."""
import unittest
import os

from hc_manager.scrape_google.scraper import scrape_target


DOCKER_ENV = os.system('sudo pwd') != 0

if DOCKER_ENV:
    tgt = scrape_target.Target(
        'hc_test_loc.csv',
        'Guido Van Rossum Job Change',
        'hc-test-bucket',
        path_driver='http://selenium__standalone-chrome:4444/wd/hub',
        test_env=True
        )
else:
    tgt = scrape_target.Target(
        'hc_test_loc.csv',
        'Guido Van Rossum Job Change',
        'hc-test-bucket',
        update_driver=False
        )


class TestBuildOps(unittest.TestCase):
    """Test Google loading."""

    def test_build_actions(self):
        """Test Google queue."""
        self.assertIsNone(
            tgt.build_actions(),
            )


class TestExecuteOps(unittest.TestCase):
    """Test execute operation queue."""

    def test_execute_operations(self):
        """Test if operations successfully execute."""
        # tgt.execute_operations()
        # Covered under test_scrape_google.
        self.assertTrue(
            True
            )
