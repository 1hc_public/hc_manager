"""Google scrape tests."""
import unittest
import os

from selenium import webdriver

from hc_manager.scrape_google.scraper import scrape_target


DOCKER_ENV = True if os.system('sudo pwd') != 0 else False

if DOCKER_ENV:
    tgt = scrape_target.Target(
        'hc_test_loc.csv',
        'Guido Van Rossum Job Change',
        'hc-test-bucket',
        path_driver='http://selenium__standalone-chrome:4444/wd/hub',
        test_env=True,
        )
else:
    tgt = scrape_target.Target(
        'hc_test_loc.csv',
        'Guido Van Rossum Job Change',
        'hc-test-bucket',
        update_driver=False,
        )

tgt.init_driver()


class TestLoadGoogle(unittest.TestCase):
    """Test Google loading."""

    def test_load_google(self):
        """Test if Google loads."""
        tgt.load_google()
        self.assertEqual(
            tgt.driver.current_url,
            'https://www.google.com/'
            )


class TestGetInput(unittest.TestCase):
    """Test if we are getting search input form."""

    def test_get_search_input(self):
        """Test if we got web element."""
        tgt.get_search_input()
        self.assertIsInstance(
            tgt.form,
            webdriver.remote.webelement.WebElement
            )


class TestSearch(unittest.TestCase):
    """Test actual search functionality."""

    def test_search_text(self):
        """Test if actually searched."""
        tgt.search_text()
        current_url = tgt.driver.current_url
        self.assertIn(
            'google.com/search',
            current_url
            )


class TestGetSummaries(unittest.TestCase):
    """Test if we are able to collect page corpus."""

    def test_get_page_summaries(self):
        """Test corpus is list."""
        tgt.get_page_summaries()
        self.assertIsInstance(
            tgt.corpus,
            list
            )


class TestCleanseSummaries(unittest.TestCase):
    """Test if cleansing properly."""

    def test_cleanse_page_summaries(self):
        """Test if corpus populated."""
        tgt.cleanse_page_summaries()
        self.assertEqual(
            tgt.data.columns.tolist(),
            ['google_index', 'corpus', 'url']
            )


class TestCheckRequirements(unittest.TestCase):
    """Test if data meets requirements."""

    def test_check_requirements(self):
        tgt.check_requirements()
        self.assertEqual(
            tgt.data.empty,
            False
            )


class TestCloseDriver(unittest.TestCase):
    """Close out."""

    def test_close(self):
        """Close webdriver."""
        self.assertIsNone(
            tgt.driver.close()
            )

# TODO: Test data write.
