.. hc_scraper documentation master file, created by
   sphinx-quickstart on Thu Feb 18 01:12:35 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to hc_manager's documentation!
=======================================

.. toctree::
   :maxdepth: 4

Manager (Distributed)
=====================

.. autoclass:: hc_manager.manager.distributed_manager.Manager
   :members:


Scrape
======

.. autoclass:: hc_manager.scrape.base.Scrape
   :members:
